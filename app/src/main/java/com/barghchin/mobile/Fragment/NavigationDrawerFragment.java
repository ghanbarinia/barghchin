package com.barghchin.mobile.Fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.barghchin.mobile.activity.CategoriesActivity;
import com.barghchin.mobile.activity.LogInActivity;
import com.barghchin.mobile.R;
import com.barghchin.mobile.utility.Utils;
import com.barghchin.mobile.utility.mApplication;

import java.lang.reflect.Field;

public class NavigationDrawerFragment extends Fragment implements View.OnClickListener {

    private Activity mActivity;
    private DrawerLayout mDrawerLayout;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mDrawerRelativeLayout = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        initializeControlls(mDrawerRelativeLayout);
        return mDrawerRelativeLayout;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.LogInActivity_Layout_Logo:
                startActivity(new Intent(mActivity, LogInActivity.class));
                break;
            case R.id.Layout_Menu_Categories:
                startActivity(new Intent(mActivity, CategoriesActivity.class));
                break;
            default:
                Utils.Toast("Click");
                break;
        }
        mDrawerLayout.closeDrawers();
    }

    private void initializeControlls(View view) {
        mActivity = getActivity();
        view.findViewById(android.R.id.content).setLayoutParams(new LinearLayout.LayoutParams((int) (mApplication.mPoint.x * 0.78), LinearLayout.LayoutParams.MATCH_PARENT));

        TextView mTV_MenuHome = (TextView) view.findViewById(R.id.TV_Menu_Home);
        TextView mTV_MenuBookmarks = (TextView) view.findViewById(R.id.TV_Menu_Bookmarks);
        TextView mTV_MenuShoppingBag = (TextView) view.findViewById(R.id.TV_Menu_ShoppingBag);

        mTV_MenuHome.setTypeface(mApplication.IRANSans);
        mTV_MenuBookmarks.setTypeface(mApplication.IRANSans);
        mTV_MenuShoppingBag.setTypeface(mApplication.IRANSans);

        view.findViewById(R.id.LogInActivity_Layout_Logo).setOnClickListener(this);
//        view.findViewById(R.id.Layout_Menu_Home).setOnClickListener(this);
        view.findViewById(R.id.Layout_Menu_Categories).setOnClickListener(this);
        view.findViewById(R.id.Layout_Menu_Bookmarks).setOnClickListener(this);
        view.findViewById(R.id.Layout_Menu_ShoppingBag).setOnClickListener(this);
        view.findViewById(R.id.Layout_Menu_PurchaseHistory).setOnClickListener(this);
        view.findViewById(R.id.Layout_Menu_Best_ellingProducts).setOnClickListener(this);
        view.findViewById(R.id.Layout_Menu_MostViewedProducts).setOnClickListener(this);
        view.findViewById(R.id.Layout_Menu_LatestProducts).setOnClickListener(this);
    }


    public void setUp(DrawerLayout drawerLayout) {
        mDrawerLayout = drawerLayout;
        getOverflowMenu();
    }

    private void getOverflowMenu() {
        try {
            ViewConfiguration config = ViewConfiguration.get(getActivity().getApplicationContext());
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
