package com.barghchin.mobile.utility;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Set;

public class MySharedPreferences {

    private static final String PREF_NAME = "com.barghchin.mobile";
    public static final String
            STORE_NAME_APP = "storeNameApp",
            STORE_COLOR = "storeColor",
            CONSUMER_KEY = "consumerKey",
            CONSUMER_SECRET = "consumerSecret",
            STORE_LOGO_APP = "storeLogoApp",
            SETTING_VERSION = "settingVersion";


    private static MySharedPreferences singletonInstance;
    private SharedPreferences sharedPref = null;

    private MySharedPreferences(Context context) {
        sharedPref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static synchronized void initializeInstance(Context context) {
        if (singletonInstance == null) {
            singletonInstance = new MySharedPreferences(context);
        }
    }

    public static synchronized MySharedPreferences getInstance() {
        if (singletonInstance == null) {
            throw new IllegalStateException(MySharedPreferences.class.getSimpleName() + " is not initialized, call initializeInstance(..) method first.");
        }
        return singletonInstance;
    }

    public void setPreference(String key, Object value) {
        SharedPreferences.Editor editor = sharedPref.edit();
        if (value instanceof Integer)
            editor.putInt(key, ((Integer) value).intValue());
        else if (value instanceof String)
            editor.putString(key, (String) value);
        else if (value instanceof Boolean)
            editor.putBoolean(key, (Boolean) value);
        else if (value instanceof Long)
            editor.putLong(key, (Long) value);
        else if (value instanceof Set)
            editor.putStringSet(key, (Set<String>) value);
        editor.commit();
    }

    public int getInt(String key, int defaultValue) {
        return sharedPref.getInt(key, defaultValue);
    }

    public boolean getcontains(String key, Object defaultValue) {
        boolean defaultContains = false;
        if (defaultValue instanceof String) {
            defaultContains = !Utils.isEmpty(getString(key, ""));
        }
        return (sharedPref.contains(key) && defaultContains);
    }

    public String getString(String key, String defaultValue) {
        return sharedPref.getString(key, defaultValue);
    }

    public boolean getBoolean(String key, boolean defaultValue) {
        return sharedPref.getBoolean(key, defaultValue);
    }

    public long getLong(String key, long defaultValue) {
        return sharedPref.getLong(key, defaultValue);
    }

    public Set getSet(String key, Set defaultValue) {
        return sharedPref.getStringSet(key, defaultValue);
    }

    public void RemovingAllPreference() {
        sharedPref.edit().clear().commit();
    }

    public void RemovingSinglePreference(String key) {
        sharedPref.edit().remove(key).commit();
    }
}
