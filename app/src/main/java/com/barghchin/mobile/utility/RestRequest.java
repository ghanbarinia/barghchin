package com.barghchin.mobile.utility;

import android.support.v4.util.ArrayMap;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.barghchin.mobile.ModelData.VolleyResponseModel;
import com.barghchin.mobile.R;

import java.util.Map;

/**
 * Created by Ghanbarinia on 7/2/2017.
 */

public class RestRequest {

    private static RestRequest instance;

    public static RestRequest getInstance() {
        if (instance == null) {
            instance = new RestRequest();
        }
        return instance;
    }

    public void callService(final String url, final callbackService callback) {
        callService(url, Request.Method.GET, null, callback);
    }

    public void callService(final String url, final int method, final callbackService callback) {
        callService(url, method, null, callback);
    }

    public void callService(final String url, final int method, final Map<String, String> params, final callbackService callback) {
        final VolleyResponseModel responseModel = new VolleyResponseModel();
        if (Utils.checkInternetConnection(false)) {
            StringRequest strReq = new StringRequest(method, url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    responseModel.setResponse(Utils.nullStrToEmpty(response));
                    if (callback != null) {
                        callback.success(responseModel);
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    NetworkResponse networkResponse = error.networkResponse;
                    if (networkResponse != null && networkResponse.data != null) {
                        try {
                            responseModel.setStatusCode(networkResponse.statusCode);
                            responseModel.setResponse(new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers)));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    responseModel.setException(error);
                    if (callback != null) {
                        callback.error(responseModel);
                    }
                }
            }) {
                protected Map<String, String> getParams() throws AuthFailureError {
                    if (params == null) {
                        return new ArrayMap<>();
                    }
                    return params;
                }
            };

            strReq.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {
                    Log.e("Alireza","error time out");
                }
            });
            mApplication.getInstance().addToRequestQueue(strReq);
        } else {
            responseModel.setResponse(null);
            responseModel.setException(new VolleyError(mApplication.applicationContext.getString(R.string.no_connection)));
            if (callback != null) {
                callback.error(responseModel);
            }
        }
    }
}
