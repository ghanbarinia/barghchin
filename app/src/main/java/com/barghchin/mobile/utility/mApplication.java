package com.barghchin.mobile.utility;


import android.app.Application;
import android.content.Context;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Handler;
import android.view.Display;
import android.view.WindowManager;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.barghchin.mobile.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class mApplication extends Application {

    private static mApplication mInstance;
    public static Point mPoint;
    public static Typeface IRANSansBold, IRANSans;
    private RequestQueue mRequestQueue;
    public static volatile Context applicationContext;
    public static volatile Handler applicationHandler;

    @Override
    public void onCreate() {
        if (applicationContext == null) {
            applicationContext = getApplicationContext();
        }
        super.onCreate();
        applicationHandler = new Handler(getApplicationContext().getMainLooper());
        mInstance = this;
        MySharedPreferences.initializeInstance(getApplicationContext());

        WindowManager wm = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        mPoint = new Point();
        display.getSize(mPoint);

        IRANSans = Typeface.createFromAsset(getAssets(), "fonts/IRANSansMobile.ttf");
        IRANSansBold = Typeface.createFromAsset(getAssets(), "fonts/IRANSansMobile_Bold.ttf");

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/IRANSansMobile.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

    public static synchronized mApplication getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(mApplication.class.getSimpleName());
        getRequestQueue().add(req);
    }

}
