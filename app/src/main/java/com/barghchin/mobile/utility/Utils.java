package com.barghchin.mobile.utility;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatDrawableManager;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.barghchin.mobile.R;


public class Utils {
    public static void Toast(String text) {
        Toast(text, Toast.LENGTH_LONG);
    }

    public static void Toast(int text) {
        Toast(mApplication.getInstance().getString(text), Toast.LENGTH_LONG);
    }

    public static void Toast(String text, int duration) {
        if (isEmpty(text)) {
            return;
        }
        if (Looper.myLooper() == Looper.getMainLooper()) {
            TextView tv = new TextView(mApplication.getInstance().getApplicationContext());
            tv.setLayoutParams(new LinearLayout.LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT, android.view.ViewGroup.LayoutParams.MATCH_PARENT));
            tv.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
            tv.setText(text);
            tv.setPadding(10, 10, 10, 10);
            //		tv.setBackgroundColor(Color.BLACK);
            tv.setTypeface(mApplication.IRANSans);
            tv.setTextSize(12f);
            tv.setBackgroundResource(R.color.toast_background);
            tv.setTextColor(Color.WHITE);

            Toast toast = new Toast(mApplication.getInstance().getApplicationContext());
            toast.setView(tv);
            toast.setDuration(duration);
            //		toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }

    public static boolean checkInternetConnection(boolean toast) {
        Context context = mApplication.applicationContext;
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connectivity.getActiveNetworkInfo();
        boolean isConnected = netInfo != null && netInfo.isConnectedOrConnecting() && netInfo.isAvailable();
        if (!isConnected && toast) {
            Toast(context.getString(R.string.no_connection));
        }
        return isConnected;
    }

    public static String nullStrToEmpty(Object str) {
        return (str == null ? "" : (str instanceof String ? (String) str : str.toString()));
    }

    public static MaterialDialog.Builder CustomDialog(@NonNull Context context) {
        return new MaterialDialog.Builder(context).titleGravity(GravityEnum.END).contentGravity(GravityEnum.START).btnStackedGravity(GravityEnum.END).itemsGravity(GravityEnum.END).buttonsGravity(GravityEnum.START).typeface(mApplication.IRANSans, mApplication.IRANSans);
    }

    public static boolean isEmpty(String str) {
        return (str == null || str.trim().equals("null") || str.trim().length() <= 0);
    }

    public static int getColor(int id) {
        return ContextCompat.getColor(mApplication.applicationContext, id);
    }

    public static Drawable getDrawable(int id) {
        return AppCompatDrawableManager.get().getDrawable(mApplication.applicationContext, id);
    }

    public static int getAppThemeColor() {
        String color = MySharedPreferences.getInstance().getString(MySharedPreferences.STORE_COLOR, mApplication.applicationContext.getString(R.color.colorPrimary));
        return Color.parseColor(color);
    }
}
