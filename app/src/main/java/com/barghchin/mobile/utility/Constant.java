package com.barghchin.mobile.utility;

import com.barghchin.mobile.R;

/**
 * Created by Ghanbarinia on 7/9/2017.
 */

public class Constant {
    public static final float AspectRatio = 0.5625f;//16:9
    public static final String urlSetting = mApplication.applicationContext.getString(R.string.DomainApp) + mApplication.applicationContext.getString(R.string.urlSetting, mApplication.applicationContext.getString(R.string.pluginName)) + mApplication.applicationContext.getString(R.string.KeySucret);

    public static final String URL_Best_sellingProducts = "http://boilerplate.ir/wp-json/wc/v2/reports/top_sellers";
    public static final String URL_AuctionProducts = "http://boilerplate.ir/wp-json/wc/v2/products?orderby=date&order=asc&on_sale=true";
    public static final String URL_NewProducts = "http://boilerplate.ir/wp-json/wc/v2/products?orderby=date&order=asc";
    public static final String URL_SpecialCategoryProducts = "http://boilerplate.ir/wp-json/wc/v2/products?orderby=date&order=asc&category=";
    public static final String URL_FeaturedProducts = "http://boilerplate.ir/wp-json/wc/v2/products?orderby=date&order=asc&featured=true";
    public static final String URL_Categories = mApplication.applicationContext.getString(R.string.urlCategorys, 0, 1);
}
