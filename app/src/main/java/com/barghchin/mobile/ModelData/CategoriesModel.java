package com.barghchin.mobile.ModelData;

import android.text.TextUtils;

/**
 * Created by Ghanbarinia on 7/18/2017.
 */

public class CategoriesModel {
    private int id;
    private String name, image;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        if (TextUtils.isEmpty(image)) {
            return "error";
        }
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
