package com.barghchin.mobile.ModelData;

import com.android.volley.VolleyError;

public class VolleyResponseModel {

    private String response;
    private VolleyError exception;
    private int statusCode;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public VolleyError getException() {
        return exception;
    }

    public void setException(VolleyError exception) {
        this.exception = exception;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }
}
