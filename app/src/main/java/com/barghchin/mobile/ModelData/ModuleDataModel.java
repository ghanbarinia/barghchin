package com.barghchin.mobile.ModelData;

import android.text.TextUtils;

import java.io.Serializable;


public class ModuleDataModel implements Serializable {
    private String id, image, title, price,regular_price,short_description;
    private int type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        if (TextUtils.isEmpty(image)) {
            return "error";
        }
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        if (TextUtils.isEmpty(price)){
            return "0";
        }
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getRegular_price() {
        if (TextUtils.isEmpty(regular_price)){
            return "0";
        }
        return regular_price;
    }

    public void setRegular_price(String regular_price) {
        this.regular_price = regular_price;
    }

    public String getShort_description() {
        return short_description;
    }

    public void setShort_description(String short_description) {
        this.short_description = short_description;
    }
}
