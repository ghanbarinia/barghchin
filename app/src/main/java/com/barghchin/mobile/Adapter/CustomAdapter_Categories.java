package com.barghchin.mobile.Adapter;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.barghchin.mobile.ModelData.MainActivityModel;
import com.barghchin.mobile.R;
import com.barghchin.mobile.activity.ProductCategoryActivity;

class CustomAdapter_Categories extends RecyclerView.Adapter<CustomAdapter_Categories.ViewHolder> {

    private Activity mActivity;
    private MainActivityModel mDataModel;

    CustomAdapter_Categories(Activity act, MainActivityModel data) {
        this.mActivity = act;
        this.mDataModel = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_categories, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("RecyclerView")
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mTitle.setText(mDataModel.getModule_data().get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        if (mDataModel == null || mDataModel.getModule_data() == null) {
            return 0;
        }
        return mDataModel.getModule_data().size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTitle;

        ViewHolder(View view) {
            super(view);
            assignViews();
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mActivity, ProductCategoryActivity.class);
                    intent.putExtra("CategoryId", mDataModel.getModule_data().get(getAdapterPosition()).getId());
                    intent.putExtra("CategoryTitle", mDataModel.getModule_data().get(getAdapterPosition()).getTitle());
                    mActivity.startActivity(intent);
                }
            });
        }

        private View findViewById(final int id) {
            return itemView.findViewById(id);
        }

        private void assignViews() {
            mTitle = (TextView) findViewById(R.id.title);
        }
    }
}
