package com.barghchin.mobile.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.barghchin.mobile.Library.viewPager.LoopViewPager;
import com.barghchin.mobile.ModelData.MainActivityModel;
import com.barghchin.mobile.R;
import com.barghchin.mobile.activity.ProductDetailsActivity;
import com.barghchin.mobile.utility.Constant;
import com.barghchin.mobile.utility.Utils;
import com.barghchin.mobile.utility.mApplication;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import me.relex.circleindicator.CircleIndicator;


public class MainActivityAdapter extends RecyclerView.Adapter<MainActivityAdapter.ViewHolder> {

    private Activity mActivity;
    private List<MainActivityModel> mDataModel;

    public MainActivityAdapter(Activity mActivity, List<MainActivityModel> datalist) {
        this.mActivity = mActivity;
        mDataModel = datalist;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPES.Category) {
            View view = LayoutInflater.from(mActivity).inflate(R.layout.category_view, parent, false);
            return new ViewHolder_Category(view);

        } else if (viewType == VIEW_TYPES.Category_Product_Linear) {
            return new ViewHolder_Product_Category(new RecyclerView(mActivity));

        } else if (viewType == VIEW_TYPES.Slider) {
            View view = LayoutInflater.from(mActivity).inflate(R.layout.slider_view, parent, false);
            return new ViewHolder_SLIDER(view);

        } else if (viewType == VIEW_TYPES.SmallBanner) {
            View view = LayoutInflater.from(mActivity).inflate(R.layout.small_banner_view, parent, false);
            return new ViewHolder_Banner_Small(view);

        } else if (viewType == VIEW_TYPES.GreatBanner) {
            View view = LayoutInflater.from(mActivity).inflate(R.layout.great_banner_view, parent, false);
            return new ViewHolder_Banner_Great(view);

        } else if (viewType == VIEW_TYPES.Best_sellingProducts || viewType == VIEW_TYPES.AuctionProducts || viewType == VIEW_TYPES.NewProducts || viewType == VIEW_TYPES.SpecialCategoryProducts || viewType == VIEW_TYPES.FeaturedProducts) {
            View view = LayoutInflater.from(mActivity).inflate(R.layout.amazing_suggestion_view, parent, false);
            return new ViewHolder_Horizontal_LIST(view);

        } else {
            return new VIEWHOLDER_EMPTY(new View(mActivity));
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mDataModel.get(position).getType();
    }

    @SuppressLint("RecyclerView")
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        if (holder.getItemViewType() == VIEW_TYPES.Category) {
            final ViewHolder_Category holder_content = (ViewHolder_Category) holder;
            holder_content.mProgressBar.setVisibility(View.VISIBLE);
            if (mDataModel.get(position).getModule_data() != null) {
                holder_content.mProgressBar.setVisibility(View.GONE);
            }
            CustomAdapter_Categories adapter = new CustomAdapter_Categories(mActivity, mDataModel.get(position));
            holder_content.mRecyclerView.setAdapter(adapter);

        } else if (holder.getItemViewType() == VIEW_TYPES.Category_Product_Linear) {
            ((RecyclerView) ((ViewHolder_Product_Category) holder).itemView).setAdapter(new CustomAdapter_Categories_Product_Linear(mActivity, mDataModel.get(position)));

        } else if (holder.getItemViewType() == VIEW_TYPES.Slider) {
            final ViewHolder_SLIDER holder_content = (ViewHolder_SLIDER) holder;
            holder_content.mViewPager.setAdapter(new SliderAdapter(mActivity, mDataModel.get(position)));
            holder_content.mIndicator.setViewPager(holder_content.mViewPager);

        } else if (holder.getItemViewType() == VIEW_TYPES.SmallBanner) {
            final ViewHolder_Banner_Small holder_content = (ViewHolder_Banner_Small) holder;
            holder_content.mProgressBar.setVisibility(View.VISIBLE);
            Picasso.with(mActivity)
                    .load(mDataModel.get(position).getImageUrl())
                    .error(Utils.getDrawable(R.drawable.place_holder))
                    .into(holder_content.mImageView1, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder_content.mProgressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            holder_content.mProgressBar.setVisibility(View.GONE);
                        }
                    });

            holder_content.mProgressBar2.setVisibility(View.VISIBLE);
            Picasso.with(mActivity)
                    .load(mDataModel.get(position).getImageUrl2())
                    .error(Utils.getDrawable(R.drawable.place_holder))
                    .into(holder_content.mImageView2, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder_content.mProgressBar2.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            holder_content.mProgressBar2.setVisibility(View.GONE);
                        }
                    });

        } else if (holder.getItemViewType() == VIEW_TYPES.GreatBanner) {
            final ViewHolder_Banner_Great holder_content = (ViewHolder_Banner_Great) holder;
            holder_content.mProgressBar.setVisibility(View.VISIBLE);
            Picasso.with(mActivity)
                    .load(mDataModel.get(position).getImageUrl())
                    .error(Utils.getDrawable(R.drawable.place_holder))
                    .into(holder_content.mImageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder_content.mProgressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            holder_content.mProgressBar.setVisibility(View.GONE);
                        }
                    });
        } else {
            final ViewHolder_Horizontal_LIST holder_content = (ViewHolder_Horizontal_LIST) holder;
            if (mDataModel.get(position).getModule_data() != null) {
                holder_content.mProgressBar.setVisibility(View.GONE);
            }
            holder_content.mTitle.setText(mDataModel.get(position).getTitle());
            CustomAdapter_HORIZONTAL_VERTICAL adapter = new CustomAdapter_HORIZONTAL_VERTICAL(mActivity, mDataModel.get(position));
            holder_content.mRecyclerView.setAdapter(adapter);
        }
    }

    @Override
    public int getItemCount() {
        if (mDataModel == null) {
            return 0;
        }
        return mDataModel.size();
    }

    public class VIEW_TYPES {
        public static final int
                SmallBanner = 1,
                GreatBanner = 2,
                Best_sellingProducts = 3,
                AuctionProducts = 4,
                NewProducts = 5,
                SpecialCategoryProducts = 6,
                FeaturedProducts = 7,
                Slider = 8,
                Category = 9,
                Category_Product_Linear = 10;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View v) {
            super(v);
        }
    }

    private class VIEWHOLDER_EMPTY extends ViewHolder {
        VIEWHOLDER_EMPTY(View v) {
            super(v);
            v.setLayoutParams(new RecyclerView.LayoutParams(0, 0));
        }
    }

    private class ViewHolder_SLIDER extends ViewHolder {

        private LoopViewPager mViewPager;
        private CircleIndicator mIndicator;

        ViewHolder_SLIDER(View view) {
            super(view);
            assignViews();
            view.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, mApplication.mPoint.x / 2));
        }

        private View findViewById(final int id) {
            return itemView.findViewById(id);
        }

        private void assignViews() {
            mViewPager = (LoopViewPager) findViewById(R.id.viewpager);
            mIndicator = (CircleIndicator) findViewById(R.id.indicator);
        }
    }

    private class ViewHolder_Horizontal_LIST extends ViewHolder {

        private RecyclerView mRecyclerView;
        private TextView mTitle, mLoadMore;
        private ProgressBar mProgressBar;

        ViewHolder_Horizontal_LIST(View view) {
            super(view);
            assignViews();

            mProgressBar.getIndeterminateDrawable().setColorFilter(Utils.getAppThemeColor(), PorterDuff.Mode.MULTIPLY);

            mTitle.setTypeface(mApplication.IRANSansBold);

            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, true));

            mLoadMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Utils.Toast(getAdapterPosition() + "");
                }
            });
        }

        private View findViewById(final int id) {
            return itemView.findViewById(id);
        }

        private void assignViews() {
            mTitle = (TextView) findViewById(R.id.TV_Title);
            mLoadMore = (TextView) findViewById(R.id.TV_LoadMore);
            mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
            mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        }
    }

    private class ViewHolder_Product_Category extends ViewHolder {

        ViewHolder_Product_Category(RecyclerView mRecyclerView) {
            super(mRecyclerView);

            mRecyclerView.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setOverScrollMode(RecyclerView.OVER_SCROLL_NEVER);
            mRecyclerView.setVerticalScrollBarEnabled(false);
            mRecyclerView.setHorizontalScrollBarEnabled(false);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));


            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, true));
        }

    }

    private class ViewHolder_Category extends ViewHolder {

        private RecyclerView mRecyclerView;
        private ProgressBar mProgressBar;

        private View findViewById(final int id) {
            return itemView.findViewById(id);
        }

        private void assignViews() {
            mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView_category_view);
            mProgressBar = (ProgressBar) findViewById(R.id.Pb_Center);
        }

        ViewHolder_Category(View view) {
            super(view);
            assignViews();

            mProgressBar.getIndeterminateDrawable().setColorFilter(Utils.getAppThemeColor(), PorterDuff.Mode.MULTIPLY);

            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setOverScrollMode(RecyclerView.OVER_SCROLL_NEVER);
            mRecyclerView.setVerticalScrollBarEnabled(false);
            mRecyclerView.setHorizontalScrollBarEnabled(false);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));


            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, true));
        }

    }

    private class ViewHolder_Banner_Great extends ViewHolder {

        private ImageView mImageView;
        private ProgressBar mProgressBar;

        ViewHolder_Banner_Great(View view) {
            super(view);
            assignViews();
            view.setLayoutParams(new RecyclerView.LayoutParams(mApplication.mPoint.x, (int) (mApplication.mPoint.x * Constant.AspectRatio)));
            mProgressBar.getIndeterminateDrawable().setColorFilter(Utils.getAppThemeColor(), PorterDuff.Mode.MULTIPLY);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mActivity.startActivity(new Intent(mActivity, ProductDetailsActivity.class).putExtra("ID", mDataModel.get(getAdapterPosition()).getLinkId()));
                }
            });
        }

        private View findViewById(final int id) {
            return itemView.findViewById(id);
        }

        private void assignViews() {
            mImageView = (ImageView) findViewById(R.id.image);
            mProgressBar = (ProgressBar) findViewById(R.id.Pb_Center);
        }
    }

    private class ViewHolder_Banner_Small extends ViewHolder {

        private ImageView mImageView1, mImageView2;
        private ProgressBar mProgressBar, mProgressBar2;

        ViewHolder_Banner_Small(View view) {
            super(view);
            assignViews();

            view.setLayoutParams(new RecyclerView.LayoutParams(mApplication.mPoint.x, (int) (mApplication.mPoint.x * Constant.AspectRatio)));

            mProgressBar.getIndeterminateDrawable().setColorFilter(Utils.getAppThemeColor(), PorterDuff.Mode.MULTIPLY);
            mProgressBar2.getIndeterminateDrawable().setColorFilter(Utils.getAppThemeColor(), PorterDuff.Mode.MULTIPLY);

            mImageView1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mActivity.startActivity(new Intent(mActivity, ProductDetailsActivity.class).putExtra("ID", mDataModel.get(getAdapterPosition()).getLinkId()));
                }
            });

            mImageView2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mActivity.startActivity(new Intent(mActivity, ProductDetailsActivity.class).putExtra("ID", mDataModel.get(getAdapterPosition()).getLinkId2()));
                }
            });
        }

        private View findViewById(final int id) {
            return itemView.findViewById(id);
        }

        private void assignViews() {
            mImageView1 = (ImageView) findViewById(R.id.image);
            mImageView2 = (ImageView) findViewById(R.id.image2);
            mProgressBar = (ProgressBar) findViewById(R.id.Pb_Center);
            mProgressBar2 = (ProgressBar) findViewById(R.id.Pb_Center2);
        }
    }
}
