package com.barghchin.mobile.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.barghchin.mobile.activity.UserCommentsActivity;
import com.barghchin.mobile.ModelData.UserCommentModel;
import com.barghchin.mobile.R;

import java.util.List;

/**
 * Created by Ghanbarinia on 7/18/2017.
 */

public class UserCommentsAdapter extends RecyclerView.Adapter<UserCommentsAdapter.UserCommentsViewHolder> {

    private UserCommentsActivity mActivity;
    private List<UserCommentModel> arrayList;

    public UserCommentsAdapter(UserCommentsActivity act, List<UserCommentModel> mData) {
        mActivity = act;
        arrayList = mData;
    }

    @Override
    public UserCommentsViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());
        ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.item_user_comments, viewGroup, false);
        return new UserCommentsViewHolder(mainGroup);
    }

    @Override
    public void onBindViewHolder(UserCommentsViewHolder holder, final int position) {
        holder.name.setText(arrayList.get(position).getName());
        holder.date.setText(arrayList.get(position).getDate_created());
        holder.comment.setText(arrayList.get(position).getReview());
    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    public class UserCommentsViewHolder extends RecyclerView.ViewHolder {
        public TextView name, date, comment;

        public UserCommentsViewHolder(View view) {
            super(view);
            this.name = (TextView) view.findViewById(R.id.name);
            this.date = (TextView) view.findViewById(R.id.date);
            this.comment = (TextView) view.findViewById(R.id.comment);
        }
    }
}
