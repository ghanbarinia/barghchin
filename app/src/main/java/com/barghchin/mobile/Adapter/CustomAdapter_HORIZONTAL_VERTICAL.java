package com.barghchin.mobile.Adapter;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.barghchin.mobile.ModelData.MainActivityModel;
import com.barghchin.mobile.R;
import com.barghchin.mobile.activity.ProductDetailsActivity;
import com.barghchin.mobile.utility.Utils;
import com.barghchin.mobile.utility.mApplication;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

class CustomAdapter_HORIZONTAL_VERTICAL extends RecyclerView.Adapter<CustomAdapter_HORIZONTAL_VERTICAL.ViewHolder> {

    private Activity mActivity;
    private MainActivityModel mDataModel;

    CustomAdapter_HORIZONTAL_VERTICAL(Activity act, MainActivityModel data) {
        this.mActivity = act;
        this.mDataModel = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("RecyclerView")
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mTitle.setText(mDataModel.getModule_data().get(position).getTitle());
        holder.mRegularPrice.setText(mDataModel.getModule_data().get(position).getRegular_price().concat(" تومان"));
        holder.mPrice.setText(mDataModel.getModule_data().get(position).getPrice().concat(" تومان"));
        holder.mRegularPrice.setPaintFlags(holder.mPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.mProgressBar.setVisibility(View.VISIBLE);
        Picasso.with(mActivity)
                .load(mDataModel.getModule_data().get(position).getImage())
                .error(Utils.getDrawable(R.drawable.place_holder))
                .into(holder.mImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        holder.mProgressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        holder.mProgressBar.setVisibility(View.GONE);
                    }
                });
    }

    @Override
    public int getItemCount() {
        if (mDataModel == null || mDataModel.getModule_data() == null) {
            return 0;
        }
        return mDataModel.getModule_data().size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView mImage;
        private TextView mTitle, mPrice, mRegularPrice;
        private ProgressBar mProgressBar;

        ViewHolder(View view) {
            super(view);
            assignViews();
            final int size = (int) (mApplication.mPoint.x / 2.2);
            view.setLayoutParams(new RecyclerView.LayoutParams(size, ViewGroup.LayoutParams.WRAP_CONTENT));


            mProgressBar.getIndeterminateDrawable().setColorFilter(Utils.getAppThemeColor(), PorterDuff.Mode.MULTIPLY);

            mImage.setLayoutParams(new FrameLayout.LayoutParams(size, size));

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mActivity.startActivity(new Intent(mActivity, ProductDetailsActivity.class).putExtra("ID", mDataModel.getModule_data().get(getAdapterPosition()).getId()));
                }
            });
        }

        private View findViewById(final int id) {
            return itemView.findViewById(id);
        }

        private void assignViews() {
            mImage = (ImageView) findViewById(R.id.image);
            mTitle = (TextView) findViewById(R.id.title);
            mRegularPrice = (TextView) findViewById(R.id.regular_price);
            mPrice = (TextView) findViewById(R.id.price);
            mProgressBar = (ProgressBar) findViewById(R.id.Pb_Center);
        }
    }
}
