package com.barghchin.mobile.Adapter;

import android.app.Activity;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.barghchin.mobile.ModelData.MainActivityModel;
import com.barghchin.mobile.utility.Constant;
import com.barghchin.mobile.utility.mApplication;
import com.squareup.picasso.Picasso;

public class SliderAdapter extends PagerAdapter {
    private Activity mActivity;
    private MainActivityModel mData;

    public SliderAdapter(Activity activity, MainActivityModel mainActivityModel) {
        mActivity = activity;
        mData = mainActivityModel;
    }

    @Override
    public int getCount() {
        if (mData == null) {
            return 0;
        }
        return mData.getModule_data().size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup view, int position, Object object) {
        view.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        ImageView imageView = new ImageView(mActivity);
        Picasso.with(mActivity).load(mData.getModule_data().get(position).getImage()).into(imageView);
        view.addView(imageView, mApplication.mPoint.x, (int) (mApplication.mPoint.x * Constant.AspectRatio));
        return imageView;
    }
}