package com.barghchin.mobile.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.barghchin.mobile.Adapter.UserCommentsAdapter;
import com.barghchin.mobile.ModelData.UserCommentModel;
import com.barghchin.mobile.ModelData.VolleyResponseModel;
import com.barghchin.mobile.R;
import com.barghchin.mobile.utility.RestRequest;
import com.barghchin.mobile.utility.Utils;
import com.barghchin.mobile.utility.callbackService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class UserCommentsActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView mRecyclerView;
    private MaterialDialog pDialog;
    private UserCommentsActivity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_comments);
        initView();
        LoadData();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initView() {
        mActivity = this;
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView_UserCommentsActivity);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));

        findViewById(R.id.Iv_Back).setOnClickListener(this);
        findViewById(R.id.ic_shopping_cart).setOnClickListener(this);
        findViewById(R.id.ic_menu_search).setOnClickListener(this);
        findViewById(R.id.fab_UserCommentsActivity).setOnClickListener(this);
    }

    private void LoadData() {
        if (pDialog == null) {
            pDialog = Utils.CustomDialog(mActivity).content(R.string.please_wait).progress(true, 0).show();
        } else {
            pDialog.show();
        }
        String Url = "http://digitalworld.myaccount.ir/wp-json/wc/v2/products/" + getIntent().getStringExtra("ID") + "/reviews?consumer_key=ck_1b0ee078fadd8aec2c812573f9c4ca80da0fae8e&consumer_secret=cs_e56c96d8c1c28d8c548c408059c7783f121ef3a3";
        RestRequest.getInstance().callService(Url, new callbackService() {
            @Override
            public void success(VolleyResponseModel callback) {
                List<UserCommentModel> mData = new ArrayList<>();
                try {
                    JSONArray jsonArray = new JSONArray(callback.getResponse());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        UserCommentModel commentModel = new UserCommentModel();
                        commentModel.setId(jsonObject.getInt("id"));
                        commentModel.setDate_created(jsonObject.getString("date_created"));
                        commentModel.setReview(jsonObject.getString("review"));
                        commentModel.setName(jsonObject.getString("name"));
                        mData.add(commentModel);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                mRecyclerView.setAdapter(new UserCommentsAdapter(mActivity, mData));
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
            }

            @Override
            public void error(VolleyResponseModel callback) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                Utils.Toast("error");
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_UserCommentsActivity:
                Utils.Toast("Insert Comment");
                break;
            case R.id.ic_shopping_cart:
                Utils.Toast("ic_shopping_cart");
                break;
            case R.id.ic_menu_search:
                Utils.Toast("ic_menu_search");
                break;
            case R.id.Iv_Back:
                onBackPressed();
                break;
        }
    }
}
