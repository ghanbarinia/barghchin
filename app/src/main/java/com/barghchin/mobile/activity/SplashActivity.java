package com.barghchin.mobile.activity;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.barghchin.mobile.ModelData.VolleyResponseModel;
import com.barghchin.mobile.R;
import com.barghchin.mobile.utility.Constant;
import com.barghchin.mobile.utility.MySharedPreferences;
import com.barghchin.mobile.utility.RestRequest;
import com.barghchin.mobile.utility.Utils;
import com.barghchin.mobile.utility.callbackService;
import com.barghchin.mobile.utility.mApplication;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

public class SplashActivity extends AppCompatActivity {

    private SplashActivity mActivity;
    private MaterialDialog mDialog;
    private ImageView mIvCenter;
    private ProgressBar mProgressBar;
    private View mroot;
    private TextView mTvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        initViews();
        loadData();
    }

    private void initViews() {
        mActivity = this;
        mIvCenter = (ImageView) findViewById(R.id.Iv_Center);
        mProgressBar = (ProgressBar) findViewById(R.id.Pb_Center);
        mTvTitle = (TextView) findViewById(R.id.TV_Title);
        mroot = findViewById(R.id.rootSplashActivity);

        mTvTitle.setTypeface(mApplication.IRANSansBold);

        int size = mApplication.mPoint.x / 2;
        mIvCenter.setLayoutParams(new FrameLayout.LayoutParams(size, size));

        setColor();
    }

    private void setColor() {
        mProgressBar.getIndeterminateDrawable().setColorFilter(Utils.getAppThemeColor(), PorterDuff.Mode.MULTIPLY);
        mroot.setBackgroundColor(Utils.getAppThemeColor());
    }

    private void loadData() {
        if (Utils.checkInternetConnection(false)) {
            if (mDialog == null) {
                mDialog = Utils.CustomDialog(mActivity).cancelable(false).content(R.string.please_wait).progress(true, 0).show();
            } else {
                mDialog.show();
            }
            Log.e("Alireza", "1");
            RestRequest.getInstance().callService(Constant.urlSetting, new callbackService() {
                @Override
                public void success(final VolleyResponseModel callback) {
                    Log.e("Alireza", "2");
                    if (mDialog != null && mDialog.isShowing()) {
                        mDialog.dismiss();
                    }
                    try {
                        JSONObject jsonObject = new JSONObject(callback.getResponse());
                        if (jsonObject.has("error") && !jsonObject.isNull("error") && jsonObject.getBoolean("error") && jsonObject.has("errorText") && !jsonObject.isNull("errorText")) {

                            Log.e("Alireza", "3");
                            Utils.CustomDialog(mActivity)
                                    .cancelable(false)
                                    .content(jsonObject.getString("errorText"))
                                    .positiveText("خروج از برنامه")
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            onBackPressed();
                                        }
                                    })
                                    .show();
                        } else {
                            Log.e("Alireza", "4");
                            int SETTING_VERSION = MySharedPreferences.getInstance().getInt(MySharedPreferences.SETTING_VERSION, 0);
                            if (SETTING_VERSION == 0 || (jsonObject.has("settingVersion") && !jsonObject.isNull("settingVersion") && jsonObject.getInt("settingVersion") != SETTING_VERSION)) {
                                Log.e("Alireza", "5");
                                if (jsonObject.has("consumer_key") && !jsonObject.isNull("consumer_key")) {
                                    MySharedPreferences.getInstance().setPreference(MySharedPreferences.CONSUMER_KEY, jsonObject.getString("consumer_key"));
                                }
                                if (jsonObject.has("consumer_secret") && !jsonObject.isNull("consumer_secret")) {
                                    MySharedPreferences.getInstance().setPreference(MySharedPreferences.CONSUMER_SECRET, jsonObject.getString("consumer_secret"));
                                }
                                if (jsonObject.has("storeNameApp") && !jsonObject.isNull("storeNameApp")) {
                                    MySharedPreferences.getInstance().setPreference(MySharedPreferences.STORE_NAME_APP, jsonObject.getString("storeNameApp"));
                                }
                                if (jsonObject.has("storeLogoApp") && !jsonObject.isNull("storeLogoApp")) {
                                    MySharedPreferences.getInstance().setPreference(MySharedPreferences.STORE_LOGO_APP, jsonObject.getString("storeLogoApp"));
                                }
                                if (jsonObject.has("storeColor") && !jsonObject.isNull("storeColor")) {
                                    MySharedPreferences.getInstance().setPreference(MySharedPreferences.STORE_COLOR, jsonObject.getString("storeColor"));
                                }
                                if (jsonObject.has("settingVersion") && !jsonObject.isNull("settingVersion")) {
                                    MySharedPreferences.getInstance().setPreference(MySharedPreferences.SETTING_VERSION, jsonObject.getInt("settingVersion"));
                                }
                            }
                            Log.e("Alireza", "6");
                            setColor();
                            mProgressBar.setVisibility(View.VISIBLE);
                            mTvTitle.setText(MySharedPreferences.getInstance().getString(MySharedPreferences.STORE_NAME_APP, ""));
                            Picasso.with(mActivity)
                                    .load(MySharedPreferences.getInstance().getString(MySharedPreferences.STORE_LOGO_APP, "error"))
                                    .error(Utils.getDrawable(R.drawable.place_holder))
                                    .into(mIvCenter, new Callback() {
                                        @Override
                                        public void onSuccess() {
                                            Log.e("Alireza", "7");
                                            mProgressBar.setVisibility(View.GONE);
                                            mApplication.applicationHandler.postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    startActivity(new Intent(mActivity, MainActivity.class).putExtra("DATA", callback.getResponse()));
                                                    finish();
                                                }
                                            }, 3000);
                                        }

                                        @Override
                                        public void onError() {
                                            Log.e("Alireza", "8");
                                            startActivity(new Intent(mActivity, MainActivity.class).putExtra("DATA", callback.getResponse()));
                                            finish();
                                        }
                                    });
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void error(VolleyResponseModel callback) {
                    Log.e("Alireza", "9");
                    if (mDialog != null && mDialog.isShowing()) {
                        mDialog.dismiss();
                    }
                }
            });
        } else {
            Utils.CustomDialog(mActivity)
                    .cancelable(false)
                    .content("اتصال شما با اینترنت برقرار نیست، آیا مایل به روشن کردن wifi یا data خود هستید؟")
                    .positiveText("بله")
                    .negativeText("خیر")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                            startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
//                            startActivity(new Intent(WifiManager.ACTION_PICK_WIFI_NETWORK));
                            startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                        }
                    })
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            onBackPressed();
                        }
                    })
                    .show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}
