package com.barghchin.mobile.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.barghchin.mobile.R;
import com.barghchin.mobile.utility.Utils;
import com.barghchin.mobile.utility.mApplication;

import static com.barghchin.mobile.R.id.ET_Name;
import static com.barghchin.mobile.R.id.ET_Phone;
import static com.barghchin.mobile.R.id.ET_key;

public class CreatingAccountActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText mETPhone, mETName, mETKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creating_account);
        initView();
    }

    private void initView() {
        mETPhone = (EditText) findViewById(ET_Phone);
        mETName = (EditText) findViewById(ET_Name);
        mETKey = (EditText) findViewById(ET_key);
        ImageView mIVRemoveRedEye = (ImageView) findViewById(R.id.IV_remove_red_eye);
        TextView mTVCreatingAccount = (TextView) findViewById(R.id.TV_Creating_account);
        RelativeLayout mLayoutCreatingAccount = (RelativeLayout) findViewById(R.id.Layout_Creating_account);
        RelativeLayout mLayoutLoginAccount = (RelativeLayout) findViewById(R.id.Layout_Login_account);
        TextView mTVLoginAccount = (TextView) findViewById(R.id.TV_Login_account);

        mETPhone.setTypeface(mApplication.IRANSans);
        mETName.setTypeface(mApplication.IRANSans);
        mETKey.setTypeface(mApplication.IRANSans);
        mTVCreatingAccount.setTypeface(mApplication.IRANSans);
        mTVLoginAccount.setTypeface(mApplication.IRANSans);

        mLayoutCreatingAccount.setOnClickListener(this);
        mLayoutLoginAccount.setOnClickListener(this);

        mIVRemoveRedEye.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    mETKey.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else if (motionEvent.getAction() == android.view.MotionEvent.ACTION_UP) {
                    mETKey.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
                return true;
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.Layout_Login_account:
                onBackPressed();
                break;

            default:
                Utils.Toast("Click");
                break;
        }
    }
}
