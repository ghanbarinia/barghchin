package com.barghchin.mobile.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.barghchin.mobile.R;
import com.barghchin.mobile.utility.Utils;
import com.barghchin.mobile.utility.mApplication;

public class LogInActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText mET_Phone, mET_key;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        initView();
    }

    private void initView() {
        View mLayoutCreatingAccount = findViewById(R.id.Layout_CreatingAccount);
        View mLayoutLoginAccount = findViewById(R.id.Layout_LoginAccount);
        mET_Phone = (EditText) findViewById(R.id.ET_Phone);
        mET_key = (EditText) findViewById(R.id.ET_key);
        TextView mTV_LoginAccount = (TextView) findViewById(R.id.TV_Login_account);
        TextView mTV_ForgetPassword = (TextView) findViewById(R.id.TV_ForgetPassword);
        TextView mTV_CreatingAccount = (TextView) findViewById(R.id.TV_CreatingAccount);

        mLayoutCreatingAccount.setOnClickListener(this);
        mLayoutLoginAccount.setOnClickListener(this);
        mTV_ForgetPassword.setOnClickListener(this);

        mET_Phone.setTypeface(mApplication.IRANSans);
        mET_key.setTypeface(mApplication.IRANSans);
        mTV_LoginAccount.setTypeface(mApplication.IRANSans);
        mTV_ForgetPassword.setTypeface(mApplication.IRANSans);
        mTV_CreatingAccount.setTypeface(mApplication.IRANSans);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.Layout_CreatingAccount:
                startActivity(new Intent(this, CreatingAccountActivity.class));
                break;
            case R.id.TV_ForgetPassword:
                DialogForgetPassword();
                break;

            default:
                Utils.Toast("Click");
                break;
        }
    }

    private void DialogForgetPassword() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_forget_password);

        TextView mTvTitle = (TextView) dialog.findViewById(R.id.TV_Title);
        TextView mTvContent = (TextView) dialog.findViewById(R.id.TV_Content);
        EditText mEtPhoneNumber = (EditText) dialog.findViewById(R.id.ET_PhoneNumber);
        TextView mTvOk = (TextView) dialog.findViewById(R.id.TV_OK);
        mTvTitle.setTypeface(mApplication.IRANSans);
        mTvContent.setTypeface(mApplication.IRANSans);
        mEtPhoneNumber.setTypeface(mApplication.IRANSans);
        mTvOk.setTypeface(mApplication.IRANSans);

        mTvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = (int) (mApplication.mPoint.x * 0.9);
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }
}
