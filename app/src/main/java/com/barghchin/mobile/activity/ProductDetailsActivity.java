package com.barghchin.mobile.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.barghchin.mobile.Adapter.SliderAdapter;
import com.barghchin.mobile.Library.viewPager.LoopViewPager;
import com.barghchin.mobile.ModelData.MainActivityModel;
import com.barghchin.mobile.ModelData.ModuleDataModel;
import com.barghchin.mobile.ModelData.VolleyResponseModel;
import com.barghchin.mobile.R;
import com.barghchin.mobile.utility.RestRequest;
import com.barghchin.mobile.utility.Utils;
import com.barghchin.mobile.utility.callbackService;
import com.barghchin.mobile.utility.mApplication;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProductDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private ProductDetailsActivity mActivity;
    private ImageView mIvBack, mIvShoppingCart;
    private MaterialDialog pDialog;
    private LoopViewPager mViewPager;
    private CircleIndicator mIndicator;
    private TextView mTvName, mTvPrice;
    private ExpandableTextView mExpandTextView;
    private RatingBar mRatingBar;
    private String URL_Share, Title_share;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        initView();
        LoadData();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initView() {
        mActivity = this;
        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        AppBarLayout mAppbarLayout = (AppBarLayout) findViewById(R.id.appbarLayout);
        mIvBack = (ImageView) findViewById(R.id.Iv_Back);
        mIvShoppingCart = (ImageView) findViewById(R.id.iv_shopping_cart);
        mViewPager = (LoopViewPager) findViewById(R.id.viewpager);
        mIndicator = (CircleIndicator) findViewById(R.id.indicator);
        findViewById(R.id.Iv_Back).setOnClickListener(this);
        findViewById(R.id.iv_shopping_cart).setOnClickListener(this);
        findViewById(R.id.Iv_Share).setOnClickListener(this);
        findViewById(R.id.Iv_Favorite).setOnClickListener(this);
        findViewById(R.id.Layout_Add_Shopping).setOnClickListener(this);
        findViewById(R.id.Layout_Comment).setOnClickListener(this);
        mTvName = (TextView) findViewById(R.id.Tv_Name);
        mRatingBar = (RatingBar) findViewById(R.id.ratingBar);
        mTvPrice = (TextView) findViewById(R.id.Tv_Price);
        mExpandTextView = (ExpandableTextView) findViewById(R.id.expand_text_view);

        collapsingToolbarLayout.setTitle(" ");

        mTvName.setTypeface(mApplication.IRANSansBold);

        mAppbarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (Math.abs(verticalOffset) - appBarLayout.getTotalScrollRange() == 0) {
                    mIvBack.setColorFilter(Color.WHITE);
                    mIvShoppingCart.setColorFilter(Color.WHITE);
                } else {
                    mIvBack.setColorFilter(Color.BLACK);
                    mIvShoppingCart.setColorFilter(Color.BLACK);
                }
            }
        });
    }

    private void LoadData() {
        if (pDialog == null) {
            pDialog = Utils.CustomDialog(mActivity).content(R.string.please_wait).progress(true, 0).show();
        } else {
            pDialog.show();
        }

        String URL = "http://digitalworld.myaccount.ir/wp-json/wc/v2/products/" + getIntent().getStringExtra("ID") + "?consumer_key=ck_1b0ee078fadd8aec2c812573f9c4ca80da0fae8e&consumer_secret=cs_e56c96d8c1c28d8c548c408059c7783f121ef3a3";
        RestRequest.getInstance().callService(URL, new callbackService() {
            @Override
            public void success(VolleyResponseModel callback) {

                MainActivityModel mainActivityModel = new MainActivityModel();
                List<ModuleDataModel> module_data = new ArrayList<>();
                mainActivityModel.setModule_data(module_data);
                try {
                    JSONObject object = new JSONObject(callback.getResponse());

                    if (object.has("name") && !object.isNull("name")) {
                        mTvName.setText(object.getString("name"));
                        Title_share = object.getString("name");
                    }


                    if (object.has("permalink") && !object.isNull("permalink")) {
                        URL_Share = object.getString("permalink");
                    }

                    if (object.has("regular_price") && !object.isNull("regular_price")) {
                        mTvPrice.setText(object.getString("regular_price").concat(" تومان"));
                    }

                    if (object.has("rating_count") && !object.isNull("rating_count")) {
                        mRatingBar.setRating(object.getInt("rating_count"));
                    }

                    if (object.has("description") && !object.isNull("description")) {
                        mExpandTextView.setText(object.getString("description"));
                    }

                    if (object.has("images") && !object.isNull("images")) {
                        JSONArray images = object.getJSONArray("images");
                        for (int i = 0; i < images.length(); i++) {
                            ModuleDataModel moduleDataModel = new ModuleDataModel();
                            moduleDataModel.setImage(images.getJSONObject(i).getString("src"));
                            module_data.add(moduleDataModel);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                mViewPager.setAdapter(new SliderAdapter(mActivity, mainActivityModel));
                mIndicator.setViewPager(mViewPager);

                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
            }

            @Override
            public void error(VolleyResponseModel callback) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                Utils.Toast("error");
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.Iv_Back:
                onBackPressed();
                break;
            case R.id.iv_shopping_cart:
                Utils.Toast("shopping");
                break;
            case R.id.Iv_Share:
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_SUBJECT, Title_share);
                i.putExtra(Intent.EXTRA_TEXT, URL_Share);
                startActivity(Intent.createChooser(i, "اشتراک گذاری :"));
                break;
            case R.id.Iv_Favorite:
                Utils.Toast("Iv_Favorite");
                break;
            case R.id.Layout_Add_Shopping:
                Utils.Toast("Layout_Add_Shopping");
                break;
            case R.id.Layout_Comment:
                startActivity(new Intent(mActivity, UserCommentsActivity.class).putExtras(getIntent()));
                break;
        }
    }
}
