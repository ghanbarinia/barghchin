package com.barghchin.mobile.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.barghchin.mobile.Adapter.MainActivityAdapter;
import com.barghchin.mobile.ModelData.MainActivityModel;
import com.barghchin.mobile.ModelData.ModuleDataModel;
import com.barghchin.mobile.ModelData.VolleyResponseModel;
import com.barghchin.mobile.R;
import com.barghchin.mobile.utility.BaseActivity;
import com.barghchin.mobile.utility.Constant;
import com.barghchin.mobile.utility.MySharedPreferences;
import com.barghchin.mobile.utility.RestRequest;
import com.barghchin.mobile.utility.Utils;
import com.barghchin.mobile.utility.callbackService;
import com.barghchin.mobile.utility.mApplication;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    private DrawerLayout mDrawer;
    private MainActivity mActivity;
    private List<MainActivityModel> mData = new ArrayList<>();
    private MainActivityAdapter mAdapter;
    private MaterialDialog pDialog;
    private RecyclerView mRecyclerView;
    private TextView mTvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView();
        initView();
        LoadData();
    }


    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(Gravity.RIGHT)) {
            mDrawer.closeDrawer(Gravity.RIGHT);
            return;
        }
        super.onBackPressed();
    }

    @SuppressLint("InflateParams")
    private void setContentView() {
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.content_main);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View activityView = layoutInflater.inflate(R.layout.activity_main, null, false);
        relativeLayout.addView(activityView);
    }

    private void initView() {
        mActivity = this;
        findViewById(R.id.TopToolbar).setBackgroundColor(Utils.getAppThemeColor());
        findViewById(android.R.id.home).setOnClickListener(this);
        findViewById(R.id.ic_shopping_cart).setOnClickListener(this);
        findViewById(R.id.ic_menu_search).setOnClickListener(this);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView_MainActivity);
        mTvTitle = (TextView) findViewById(R.id.TV_Title);

        mTvTitle.setTypeface(mApplication.IRANSansBold);

        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
        layoutManager.setAutoMeasureEnabled(true);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setLayoutManager(layoutManager);
    }

    private void LoadData() {
        try {
            mTvTitle.setText(MySharedPreferences.getInstance().getString(MySharedPreferences.STORE_NAME_APP, ""));

            JSONObject jsonObject = new JSONObject(getIntent().getStringExtra("DATA"));
            if (jsonObject.has("slider") & !jsonObject.isNull("slider")) {

                MainActivityModel slider_Model = new MainActivityModel();
                mData.add(slider_Model);

                slider_Model.setType(MainActivityAdapter.VIEW_TYPES.Slider);

                List<ModuleDataModel> moduleDataModels = new ArrayList<>();
                slider_Model.setModule_data(moduleDataModels);

                JSONArray slider = jsonObject.getJSONArray("slider");
                for (int i = 0; i < slider.length(); i++) {
                    JSONObject tempJsonObject = slider.getJSONObject(i);
                    ModuleDataModel tempDataModel = new ModuleDataModel();

                    if (tempJsonObject.has("value") && !tempJsonObject.isNull("value")) {
                        tempDataModel.setId(tempJsonObject.getString("value"));
                    }

                    if (tempJsonObject.has("type") && !tempJsonObject.isNull("type")) {
                        tempDataModel.setType(tempJsonObject.getInt("type"));
                    }

                    if (tempJsonObject.has("url") && !tempJsonObject.isNull("url")) {
                        tempDataModel.setImage(tempJsonObject.getString("url"));
                    }

                    if (tempJsonObject.has("title") && !tempJsonObject.isNull("title")) {
                        tempDataModel.setTitle(tempJsonObject.getString("title"));
                    }

                    moduleDataModels.add(tempDataModel);
                }
            }

            if (jsonObject.has("showCategory") & !jsonObject.isNull("showCategory")) {
                MainActivityModel category_Model = new MainActivityModel();
                mData.add(category_Model);
                category_Model.setType(MainActivityAdapter.VIEW_TYPES.Category);
                getProducts(category_Model.getType(), mData.size() - 1);
            }


            if (jsonObject.has("mainPage") & !jsonObject.isNull("mainPage")) {
                JSONArray mainPage = jsonObject.getJSONArray("mainPage");
                for (int i = 0; i < mainPage.length(); i++) {
                    MainActivityModel mainPage_Model = new MainActivityModel();
                    mData.add(mainPage_Model);

                    JSONObject temp_JsonObject = mainPage.getJSONObject(i);
                    if (temp_JsonObject.has("bgColor") && !temp_JsonObject.isNull("bgColor")) {
                        mainPage_Model.setBgColor(temp_JsonObject.getString("bgColor"));
                    }

                    if (temp_JsonObject.has("textColor") && !temp_JsonObject.isNull("textColor")) {
                        mainPage_Model.setTextColor(temp_JsonObject.getString("textColor"));
                    }

                    if (temp_JsonObject.has("title") && !temp_JsonObject.isNull("title")) {
                        mainPage_Model.setTitle(temp_JsonObject.getString("title"));
                    }

                    if (temp_JsonObject.has("linkId") && !temp_JsonObject.isNull("linkId")) {
                        mainPage_Model.setLinkId(temp_JsonObject.getString("linkId"));
                    }

                    if (temp_JsonObject.has("linkId2") && !temp_JsonObject.isNull("linkId2")) {
                        mainPage_Model.setLinkId2(temp_JsonObject.getString("linkId2"));
                    }

                    if (temp_JsonObject.has("itemsCount") && !temp_JsonObject.isNull("itemsCount")) {
                        mainPage_Model.setItemsCount(temp_JsonObject.getInt("itemsCount"));
                    }

                    if (temp_JsonObject.has("imageUrl") && !temp_JsonObject.isNull("imageUrl")) {
                        mainPage_Model.setImageUrl(temp_JsonObject.getString("imageUrl"));
                    }

                    if (temp_JsonObject.has("imageUrl2") && !temp_JsonObject.isNull("imageUrl2")) {
                        mainPage_Model.setImageUrl2(temp_JsonObject.getString("imageUrl2"));
                    }

                    if (temp_JsonObject.has("likeType") && !temp_JsonObject.isNull("likeType")) {
                        mainPage_Model.setLikeType(temp_JsonObject.getInt("likeType"));
                    }

                    if (temp_JsonObject.has("likeType2") && !temp_JsonObject.isNull("likeType2")) {
                        mainPage_Model.setLikeType2(temp_JsonObject.getInt("likeType2"));
                    }

                    if (temp_JsonObject.has("type") && !temp_JsonObject.isNull("type")) {
                        mainPage_Model.setType(temp_JsonObject.getInt("type"));
                    }
                    getProducts(mainPage_Model.getType(), mData.size() - 1);
                }
                mRecyclerView.setAdapter(mAdapter = new MainActivityAdapter(mActivity, mData));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getProducts(final int type, final int position) {
        String URL = null;
        switch (type) {
            case MainActivityAdapter.VIEW_TYPES.Best_sellingProducts:
                URL = Constant.URL_Best_sellingProducts;
                break;
            case MainActivityAdapter.VIEW_TYPES.AuctionProducts:
                URL = Constant.URL_AuctionProducts;
                break;
            case MainActivityAdapter.VIEW_TYPES.NewProducts:
                URL = Constant.URL_NewProducts;
                break;
            case MainActivityAdapter.VIEW_TYPES.SpecialCategoryProducts:
                URL = Constant.URL_SpecialCategoryProducts + mData.get(position).getLinkId();
                break;
            case MainActivityAdapter.VIEW_TYPES.FeaturedProducts:
                URL = Constant.URL_FeaturedProducts;
                break;
            case MainActivityAdapter.VIEW_TYPES.Category:
                URL = Constant.URL_Categories;
                break;
        }
        if (!TextUtils.isEmpty(URL)) {
            final String finalURL = URL;
            RestRequest.getInstance().callService(URL, new callbackService() {
                @Override
                public void success(VolleyResponseModel callback) {
                    try {
                        List<ModuleDataModel> module_data = new ArrayList<>();
                        mData.get(position).setModule_data(module_data);


                        JSONArray jsonArray = new JSONArray(callback.getResponse());
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            ModuleDataModel dataModel = new ModuleDataModel();

                            if (jsonObject.has("id") && !jsonObject.isNull("id")) {
                                dataModel.setId(jsonObject.getString("id"));
                            }

                            if (jsonObject.has("name") && !jsonObject.isNull("name")) {
                                dataModel.setTitle(jsonObject.getString("name"));
                            }

                            if (jsonObject.has("price") && !jsonObject.isNull("price")) {
                                dataModel.setPrice(jsonObject.getString("price"));
                            }

                            if (jsonObject.has("regular_price") && !jsonObject.isNull("regular_price")) {
                                dataModel.setRegular_price(jsonObject.getString("regular_price"));
                            }

                            if (jsonObject.has("images") && !jsonObject.isNull("images")) {
                                JSONArray images = jsonObject.getJSONArray("images");
                                if (images.getJSONObject(0).has("src") && !images.getJSONObject(0).isNull("src")) {
                                    dataModel.setImage(images.getJSONObject(0).getString("src"));
                                }
                            }
                            module_data.add(dataModel);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    mAdapter.notifyItemChanged(position);
                }

                @Override
                public void error(VolleyResponseModel callback) {
                    Log.e("Alireza" + position, "URL" + finalURL);
                }
            });
        }
    }

    private void ShowDialog() {
        if (pDialog == null) {
            pDialog = Utils.CustomDialog(mActivity).content(R.string.please_wait).progress(true, 0).show();
        } else {
            pDialog.show();
        }
    }

    private void DismissDialog() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case android.R.id.home:
                if (mDrawer.isDrawerOpen(Gravity.RIGHT)) {
                    mDrawer.closeDrawer(Gravity.RIGHT);
                } else {
                    mDrawer.openDrawer(Gravity.RIGHT);
                }
                break;

            case R.id.ic_shopping_cart:
                Utils.Toast("Shopping");
                break;

            case R.id.ic_menu_search:
                Utils.Toast("Search");
                break;
        }
    }
}
